﻿namespace WEB_API_CI_CD.Models
{
    public class AppSettings
    {
        public string DopUrl { get; set; }
        public string JwtSecretKey { get; set; }
        public int JwtMinuteExpired { get; set; } = 0;
        public string DefaultConnection { get; set; }
    }
}

﻿using LazyCache;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using WEB_API_CI_CD.Models;

namespace WEB_API_CI_CD.Service.Common
{
    public interface IDbService
    {
        Task<string> GetConnectionString(ConnectionType types);
    }

    public class DbService : IDbService
    {
        private readonly IAppCache _appCache;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<DbService> _logger;
        private readonly AppSettings _appSetings;

        public DbService(IAppCache appCache, IHttpClientFactory httpClientFactory, IOptions<AppSettings> options, ILogger<DbService> logger)
        {
            this._appCache = appCache;
            this._logger = logger;
            this._httpClientFactory = httpClientFactory;
            this._appSetings = options.Value;
        }

        #region Public 

        public async Task<string> GetConnectionString(ConnectionType types)
        {

            string cacheKey = $"GetConnectionString_{types.DisplayName()}";
            string result = string.Empty;

            result = await _appCache.GetOrAddAsync(cacheKey, cacheEntry =>
            {
                cacheEntry.SlidingExpiration = TimeSpan.FromMinutes(10);
                cacheEntry.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(300);

                return ConnectionStringApi(types.DisplayName());
            });

            if (String.IsNullOrEmpty(result))
            {
                result = await ConnectionStringApi(types.DisplayName());
            }

            return result;
        }

        #endregion

        #region Private 

        private async Task<string> ConnectionStringApi(string connectionType)
        {
            string result = string.Empty;

            try
            {
                string url = $"/DBCoreAPI/api/DBCon/service/{connectionType}";

                var client = _httpClientFactory.CreateClient("dop");
                if (client.BaseAddress == null)
                {
                    client.BaseAddress = new Uri(_appSetings.DopUrl);
                }

                using (var httpResponse = await client.GetAsync(url))
                {
                    if (httpResponse.IsSuccessStatusCode)
                    {
                        var json = await httpResponse.Content.ReadAsStringAsync();
                        result = await ConnectionStringHelper.GetString(json);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }

            return result;
        }


        #endregion
    }

    public static class ConnectionStringHelper
    {
        public static async Task<string> GetString(string json)
        {

            MainString main = JsonConvert.DeserializeObject<MainString>(json);

            if (main != null)
            {
                byte[] data = Convert.FromBase64String(main.Database.connectionString);
                return System.Text.Encoding.UTF8.GetString(data);
            }

            return string.Empty;
        }

        public class Database
        {
            public string databaseName { get; set; }
            public string connectionString { get; set; }
        }

        public class MainString
        {
            public bool result { get; set; }
            public string errorMessage { get; set; }
            public Database Database { get; set; }
        }
    }

    public enum ConnectionType
    {

        [EnumDisplayName(DisplayName = "INVAUTH_SQL")]
        INVAUTH
    }
    public enum ActionType
    {

        [EnumDisplayName(DisplayName = "ADD")]
        ADD,
        [EnumDisplayName(DisplayName = "UPDATE")]
        UPDATE,
        [EnumDisplayName(DisplayName = "DELETE")]
        DELETE,
        [EnumDisplayName(DisplayName = "REMOVE")]
        REMOVE
    }
    public enum LogType
    {
        [EnumDisplayName(DisplayName = "DEFAULT")] DEFAULT
        , [EnumDisplayName(DisplayName = "INFO")] INFO
        , [EnumDisplayName(DisplayName = "ERROR")] ERROR
        , [EnumDisplayName(DisplayName = "AUDIT")] AUDIT
    }
    public enum ActionResultString
    {

        [EnumDisplayName(DisplayName = "SUCCESS")]
        SUCCESS,
        [EnumDisplayName(DisplayName = "FAIL")]
        FAIL,
        [EnumDisplayName(DisplayName = "ERROR")]
        ERROR,
        [EnumDisplayName(DisplayName = "WARNING")]
        WARNING
    }
    public class EnumDisplayNameAttribute : Attribute
    {
        public string DisplayName { get; set; }
    }

    public static class EnumExtensions
    {
        public static string DisplayName(this Enum value)
        {
            System.Reflection.FieldInfo field = value.GetType().GetField(value.ToString());

            EnumDisplayNameAttribute attribute = Attribute.GetCustomAttribute(field, typeof(EnumDisplayNameAttribute)) as EnumDisplayNameAttribute;

            return attribute == null ? value.ToString() : attribute.DisplayName;
        }


    }
}

﻿using Microsoft.Extensions.Options;
using WEB_API_CI_CD.Models;
using System.Data.SqlClient;
using System.Data;

namespace WEB_API_CI_CD.Service.Common
{
    public interface ISqlService
    {
        Task<SqlConnection> connectsrd();
        Task<int> SqlExecute(string sql, SqlParameterCollection parameters);
        Task<int> SqlExecute(string sql);
        Task<DataSet> SqlGet(string sql, SqlParameterCollection parameters);
        Task<DataSet> SqlGet(string sql);
        Task<DataTable> SqlQuery1(string sql);
        Task<DataTable> SqlQuery(string sql);
        Task<DataSet> SqlExcStoNonparameter(string stpName);
        Task<DataSet> SqlExcSto(string stpName, SqlParameterCollection parameters);
        Task<int> SqlExcStoInt(string sql, SqlParameterCollection parameters);
        Task<DataSet> SqlExcFunctionScalar(string stpName, SqlParameterCollection parameters);
    }
    public class SqlService: ISqlService
    {
        private readonly ILogger<SqlService> _logger;
        private readonly AppSettings _appSettings;
        private readonly IDbService _dbService;
        public SqlService(ILogger<SqlService> logger, IOptions<AppSettings> options, IDbService dbService)
        {
            _logger = logger;
            _appSettings = options.Value;
            _dbService = dbService;
        }
        public async Task<SqlConnection> connectsrd()
        {
            //string constring = ConfigurationManager.AppSettings["WS_CMIS_Service"];
            SqlConnection con = new SqlConnection(_appSettings.DefaultConnection);
            return con;
        }
        #region Public

        // Execute Data Must Have a parameter
        public async Task<int> SqlExecute(string sql, SqlParameterCollection parameters)
        {
            int i;
            SqlConnection conn = await connectsrd();
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandTimeout = 0;
            foreach (SqlParameter param in parameters)
            {
                cmd.Parameters.AddWithValue(param.ParameterName, param.SqlDbType).Value = param.Value;
            }
            conn.Open();
            i = cmd.ExecuteNonQuery();
            conn.Close();
            return i;
        }

        // Execute Data Non Parameter
        public async Task<int> SqlExecute(string sql)
        {
            int i;
            SqlConnection conn = await connectsrd();
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandTimeout = 0;
            conn.Open();
            i = cmd.ExecuteNonQuery();
            conn.Close();
            return i;
        }
        // Query Must Have a parameter
        public async Task<DataSet> SqlGet(string sql, SqlParameterCollection parameters)
        {
            SqlConnection conn = await connectsrd();
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            foreach (SqlParameter param in parameters)
            {
                da.SelectCommand.Parameters.AddWithValue(param.ParameterName, param.SqlDbType).Value = param.Value;
            }
            da.Fill(ds);
            return ds;
        }

        // Query Non ParaMeter Select Data To DataSet
        public async Task<DataSet> SqlGet(string sql)
        {
            SqlConnection conn = await connectsrd();
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        // Special Query Select Data To DataTable
        public async Task<DataTable> SqlQuery1(string sql)
        {
            SqlConnection conn = await connectsrd();
            DataTable dtViewQueryData = new DataTable();
            SqlCommand sqlComm = new SqlCommand(sql, conn);
            sqlComm.CommandType = CommandType.Text;
            conn.Open();
            sqlComm.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = sqlComm;
            DataSet ds = new DataSet();
            da.Fill(ds);
            dtViewQueryData = ds.Tables[0];
            conn.Close();
            return dtViewQueryData;
        }
        // Query Select Data To DataTable
        public async Task<DataTable> SqlQuery(string sql)
        {
            SqlConnection conn = await connectsrd();
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        // Execute Stored Select Data To DataSet Non Have Parameter
        public async Task<DataSet> SqlExcStoNonparameter(string stpName)
        {
            SqlConnection conn = await connectsrd();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 0;
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = stpName;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        // Execute Stored Select Data To DataSet Must Have a parameter
        public async Task<DataSet> SqlExcSto(string stpName, SqlParameterCollection parameters)
        {
            SqlConnection conn = await connectsrd();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 0;
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = stpName;
            foreach (SqlParameter param in parameters)
            {
                cmd.Parameters.AddWithValue(param.ParameterName, param.SqlDbType).Value = param.Value;
            }
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        //Execute strore Insert, Updaet return int
        public async Task<int> SqlExcStoInt(string sql, SqlParameterCollection parameters)
        {
            int i;
            SqlConnection conn = await connectsrd();
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandTimeout = 0;
            cmd.CommandType = CommandType.StoredProcedure;
            foreach (SqlParameter param in parameters)
            {
                cmd.Parameters.AddWithValue(param.ParameterName, param.SqlDbType).Value = param.Value;
            }
            conn.Open();
            i = cmd.ExecuteNonQuery();
            conn.Close();
            return i;
        }
        //Execute Scalar Function GetData Set 
        public async Task<DataSet> SqlExcFunctionScalar(string stpName, SqlParameterCollection parameters)
        {
            using (var connection = await connectsrd())
            {
                connection.Open();
                var cmd = connection.CreateCommand();
                cmd.CommandText = stpName;
                foreach (SqlParameter param in parameters)
                {
                    cmd.Parameters.AddWithValue(param.ParameterName, param.SqlDbType).Value = param.Value;
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                cmd.Dispose();
                connection.Close();
                return ds;

            }
        }


        //Set the connection, command, and then execute the command with non query.S
        public async Task SqlCmdNonQueryAsync(SqlCommand command, ConnectionType connectionType)
        {
            try
            {
                string connString = await _dbService.GetConnectionString(connectionType);

                if (!String.IsNullOrEmpty(connString))
                {
                    using (var conn = new SqlConnection(connString))
                    {
                        if (conn.State != ConnectionState.Open) conn.Open();

                        command.Connection = conn;

                        await command.ExecuteNonQueryAsync();
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        public async Task<int> SqlCmdNonQuery(SqlCommand command, ConnectionType connectionType)
        {
            int result = 0;
            try
            {
                string connString = await _dbService.GetConnectionString(connectionType);

                if (!String.IsNullOrEmpty(connString))
                {
                    using (var conn = new SqlConnection(connString))
                    {
                        if (conn.State != ConnectionState.Open) conn.Open();

                        command.Connection = conn;

                        result = command.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
            return result;
        }

        public async Task<DataTable> SqlCmdDataTableAsync(SqlCommand command, ConnectionType connectionType)
        {
            DataTable result = new DataTable();

            try
            {
                string connString = await _dbService.GetConnectionString(connectionType);

                if (!String.IsNullOrEmpty(connString))
                {
                    using (var conn = new SqlConnection(connString))
                    {
                        if (conn.State != ConnectionState.Open) conn.Open();

                        command.Connection = conn;

                        using (var adapter = new SqlDataAdapter(command))
                            adapter.Fill(result);
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }

            return result;
        }

        public async Task<DataSet> SqlCmdDataSetAsync(SqlCommand command, ConnectionType connectionType)
        {
            DataSet result = new DataSet();

            try
            {
                string connString = await _dbService.GetConnectionString(connectionType);

                if (!String.IsNullOrEmpty(connString))
                {
                    using (var conn = new SqlConnection(connString))
                    {
                        if (conn.State != ConnectionState.Open) conn.Open();

                        command.Connection = conn;

                        using (var adapter = new SqlDataAdapter(command))
                            adapter.Fill(result);
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }

            return result;
        }

        public async Task<T> SqlCmdScalarAsync<T>(SqlCommand command, ConnectionType connectionType)
        {
            var result = default(T);

            try
            {
                string connString = await _dbService.GetConnectionString(connectionType);

                if (!String.IsNullOrEmpty(connString))
                {
                    using (var conn = new SqlConnection(connString))
                    {
                        if (conn.State != ConnectionState.Open) conn.Open();

                        command.Connection = conn;

                        var response = await command.ExecuteScalarAsync();

                        if (response != null)
                        {
                            result = (T)response;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }

            return result;
        }

        #endregion

    }
}

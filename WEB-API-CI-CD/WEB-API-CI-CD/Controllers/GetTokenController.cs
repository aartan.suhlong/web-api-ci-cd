﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using WEB_API_CI_CD.Models;
using WEB_API_CI_CD.Service;

namespace WEB_API_CI_CD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetTokenController : ControllerBase
    {
        private readonly ILogger<GetTokenController> _logger;
        private readonly IGetTokenService _getTokenService;
        private readonly AppSettings _appSettings;

        public GetTokenController(ILogger<GetTokenController> logger, IGetTokenService getTokenService, IOptions<AppSettings> options)
        {
            _logger = logger;
            _getTokenService = getTokenService;
            _appSettings = options.Value;
        }
        [Route("AuthenLogin")]
        [HttpPost]
        public async Task<IActionResult> AuthenLogin([FromBody] LoginModel loginModel)
        {
            ResGetToken res = new ResGetToken();
            string errMessage = string.Empty;
            try
            {
                if (loginModel != null)
                {
                    var result = await _getTokenService.CheckUser(loginModel);
                    if (result)
                    {
                        var issuer = _appSettings.DopUrl.ToString();
                        var audience = _appSettings.DopUrl.ToString();
                        Guid sessionID = Guid.NewGuid();
                        var tokenHandler = new JwtSecurityTokenHandler();
                        var key = System.Text.Encoding.ASCII.GetBytes(_appSettings.JwtSecretKey);
                        Claim[] claims = new Claim[]
                        {
                            new Claim("SessionID", sessionID.ToString()),
                            new Claim("Username",  loginModel.Email)                         

                        };
                        var tokenDescriptor = new SecurityTokenDescriptor
                        {
                            Subject = new ClaimsIdentity(claims),
                            Expires = DateTime.UtcNow.AddMinutes(_appSettings.JwtMinuteExpired),
                            //Issuer = issuer,
                            //Audience = audience,
                            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                        };
                        var securityToken = tokenHandler.CreateToken(tokenDescriptor);


                        var jwtToken = tokenHandler.WriteToken(securityToken);
                        var stringToken = tokenHandler.WriteToken(securityToken);
                        res = new ResGetToken() {
                            access_token = tokenHandler.WriteToken(securityToken),
                            expires_in = Convert.ToInt32(TimeSpan.FromMinutes(_appSettings.JwtMinuteExpired).TotalSeconds),
                            token_type = "Bearer",
                            result = true
                        };
                        _logger.LogInformation("Authen Success");
                        return Ok(res);
                    }
                    else
                    {
                        errMessage = "username and password are incorrect please check";
                        res = new ResGetToken()
                        {
                            errorMessage = errMessage,
                            result = false
                        };
                        _logger.LogInformation("Authen False : " + errMessage);

                        return Unauthorized(res);
                    }
                }
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
                _logger.LogError(ex.Message);
            }
            return Unauthorized(res);
        }
    }
}

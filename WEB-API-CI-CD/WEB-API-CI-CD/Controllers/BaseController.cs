﻿using WEB_API_CI_CD.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Reflection;
using System.Text.Json;

namespace WEB_API_CI_CD.Controllers
{
 
    public class BaseController : ControllerBase
    {
        public static bool IsValidJsonObject(string text)
        {
            bool result = false;
            try
            {
                JObject jObj = JObject.Parse(text);
                result = (jObj != null);
            }
            catch
            { }

            return result;
        }
        public static bool IsValidObjectOrAllModelResult(object Obj)
        {
            bool result = false;

            try
            {
                string text = ConvertObjToJsonString(Obj);
                dynamic Dnm = JsonConvert.DeserializeObject<dynamic>(text);
                result = Dnm["result"] == "true" ? true : false;
                //JObject jObj = JObject.Parse(text);

                //result = ;
            }
            catch
            {
                result = false;
            }
            return result;
        }
        public static bool IsValidJsonStringResult(string text)
        {
            bool result = false;

            try
            {
                dynamic Dnm = JsonConvert.DeserializeObject<dynamic>(text);
                result = Dnm["result"] == "true" ? true : false;
                //JObject jObj = JObject.Parse(text);

                //result = ;
            }
            catch
            {
                result = false;
            }
            return result;
        }
        public static bool IsValidDynamicResult(dynamic Dnm)
        {
            bool result = false;

            try
            {
                //dynamic Dnm = JsonConvert.DeserializeObject<dynamic>(text);
                result = Dnm["result"] == "true" ? true : false;
                //JObject jObj = JObject.Parse(text);

                //result = ;
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public static string ConvertObjToJsonString(object obj)
        {
            var options = new JsonSerializerOptions
            {
                WriteIndented = true
            };
            var JsonString = System.Text.Json.JsonSerializer.Serialize(obj, options);
            return JsonString;
        }
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        public static T DataTableToModel<T>(DataTable dataTable)
        {
            var json = JsonConvert.SerializeObject(dataTable);
            var myModels = JsonConvert.DeserializeObject<List<T>>(json);

            return myModels.FirstOrDefault();
        }
        public static List<T> DataTableToModelList<T>(DataTable dataTable)
        {
            var json = JsonConvert.SerializeObject(dataTable);
            var myModelsList = JsonConvert.DeserializeObject<List<T>>(json);

            return myModelsList;
        }
        public static string ConvertGetDateFormat(string data, string format)
        {
            try
            {
                string[] tempdata = Convert.ToDateTime(data).ToString("yyyy-MM-dd").Split('-');
                if (Convert.ToInt32(tempdata[0]) > 2500)
                {
                    tempdata[0] = Convert.ToInt32(Convert.ToInt32(tempdata[0]) - 543).ToString("0000");
                }

                format = format.ToUpper().Replace("D", Convert.ToInt32(tempdata[2]).ToString("00"));
                format = format.ToUpper().Replace("M", Convert.ToInt32(tempdata[1]).ToString("00"));
                format = format.ToUpper().Replace("Y", tempdata[0].ToString());
                return format;
            }
            catch (Exception)
            {

                return data;
            }
        }
        public static string GetActionName(ControllerContext controllerContext)
        {
            string ActionName = string.Empty;
            try
            {
                string[] ActionNameArray = controllerContext.ActionDescriptor.DisplayName.Split('.');
                ActionName = ActionNameArray[ActionNameArray.Length - 1];
                ActionName = ActionName.Substring(0, ActionName.IndexOf(' '));
            }
            catch (Exception ex)
            {
                return null;
            }
            return ActionName;
        }
        public static string GetControllerName(ControllerContext controllerContext)
        {
            string ControllerName = string.Empty;
            try
            {
                ControllerName = controllerContext.ActionDescriptor.ControllerName;
            }
            catch (Exception ex)
            {
                return null;
            }
            return ControllerName;
        }
    }
}
